##' @title [DEPRECATED] Compile data from the Peruvian National Hydrological and Meterological Service
##'
##' @description Compile a CSV file of Peruvian historical climate data from the Senamhi web portal.
##'
##' @param station character; the station id number to process.
##' @param data_dir The dir to use, default set. 
##' @param catalogue_fn The filename of the catalogue, default set.
##' 
##' @return None
##'
##' @author Conor I. Anderson
##'
##' @importFrom XML readHTMLTable
##' @importFrom tibble as_tibble has_name 
##' @importFrom utils setTxtProgressBar txtProgressBar
##'
##' @export
##'

compile_HTML <- function(station,
                         data_dir = "/data/pcd_raw/",
                         catalogue_fn = "local_catalogue.rds") {
  
  localcatalogue <- readRDS(catalogue_fn)
  
  station_data <- localcatalogue[localcatalogue$StationID == station, ]
  type = station_data$Type
  config = station_data$Configuration
  station_name <- station_data$Station
  region <- station_data$Region
  year <- station_data$`Data Start`:station_data$`Data End`
  
  # This snippet of code from Stack Overflow user Grzegorz Szpetkowski at
  # http://stackoverflow.com/questions/6243088/find-out-the-number-of-days-of-a-month-in-r
  
  number_of_days <- function(date) {
    m <- format(date, format = "%m")
    while (format(date, format = "%m") == m) {
      date <- date + 1
    }
    return(as.integer(format(date - 1, format = "%d")))
  }
  ##--------------------------------------------------------------------------------------
  
  # GenFileList
  month <- sprintf("%02d", 1:12)
  files <- apply(expand.grid(month, year), 1, function(x) paste0(x[2], x[1]))
  files <- paste0(region, "/HTML/", as.character(station), " - ", station_name, "/", files, ".html")
  
  # GenDates
  datelist <- apply(expand.grid(month, year), 1, function(x) paste(x[2], x[1], sep = "-"))
  datelist <- paste(datelist, "01", sep = "-")
  
  startDate <- as.Date(paste0(min(year), "-01-01"), format = "%Y-%m-%d")
  endDate <- as.Date(paste0(max(year), "-12-31"), format = "%Y-%m-%d")
  datecolumn <- seq(as.Date(startDate), by = "day", length.out = (as.numeric(as.Date(endDate) - as.Date(startDate)) + 1))
  
  col_names <- .clean_table(config = config, type = type, clean_names = TRUE)
  dat <- as_tibble(as_tibble(matrix(data = as.character(NA), 
                                    nrow = length(datecolumn), ncol = length(col_names))),
                   .name_repair = ~col_names)
  dat$Fecha <- datecolumn
  row <- 1
  
  print("Compiling data.")
  prog <- txtProgressBar(min = 0, max = length(files), style = 3)
  on.exit(close(prog))
  
  ## Loop through files and input data to table
  for (i in 1:length(files)) {
    date <- as.Date(datelist[i], format = "%Y-%m-%d")
    table <- try(readHTMLTable(files[i], stringsAsFactors = FALSE))
    if (inherits(table, "try-error")) {
      stop("Could not read the requested file. Are you sure you downloaded it?")
    }
    table <- as_tibble(table[[1]])
    if (nrow(table) > 1) {
      ## Sometimes the HTML files only have a few days, instead of the whole month
      if (nrow(table) - 1 != number_of_days(date)) {
        table <- table[-1,]
        for (j in 1:nrow(table)) {
          datadate <- as.character(table[j, 1])
          datadate <- strsplit(datadate, split = "-")[[1]]
          datadate <- as.numeric(datadate[1])
          thisrow <- row + datadate - 1
          if (!is.na(thisrow)) {
            if (ncol(table) != length(col_names)) {
              if (ncol(table) == 3 & config == "M") {
                # Sometimes we get tables that only have precipitation
                dat$`Prec07 (mm)`[thisrow] <- unlist(table[j, 2])
                dat$`Prec19 (mm)`[thisrow] <- unlist(table[j, 3])
              } else if (ncol(table) == 10 && length(col_names) == 9) {
                # Sometimes we randomly get river data at a met station. Discard for now.
                table <- table[,-10]
                dat[thisrow, 2:ncol(dat)] <- table[j,2:ncol(table)]
              } else if (ncol(table) == 5 & config == "H") {
                # Sometimes hydro stations are missing the flow rate column at the end
                dat[thisrow, 2:ncol(table)] <- table[j, 2:ncol(table)]
              } else {
                stop("We have a strange situation that needs manual debugging!")
              }
            } else {
              dat[thisrow, 2:length(dat)] <- table[j, 2:ncol(table)]  
            }
          }
        }
      } else {
        # Sometimes the HTML files only have a subset of the columns
        if (ncol(table) != length(col_names)) {
          if (ncol(table) == 10 && length(col_names) == 9) {
            # Sometimes we randomly get river data at a met station. Discard for now.
            table <- table[,-10]
            dat[row:(row + number_of_days(date) - 1), 2:ncol(dat)] <- table[2:nrow(table),2:ncol(table)]
          } else if (ncol(table) == 5 & config == "H") {
            # Sometimes hydro stations are missing the flow rate column at the end
            dat[row:(row + number_of_days(date) - 1), 2:ncol(table)] <- table[2:nrow(table),2:ncol(table)]
          } else {
            # Sometimes we get tables that only have precipitation
            table <- table[-1, ]
            dat$`Prec07 (mm)`[row:(row + nrow(table) - 1)] <- table[[2]]
            dat$`Prec19 (mm)`[row:(row + nrow(table) - 1)] <- table[[3]]
          }
        } else {
          dat[row:(row + number_of_days(date) - 1), 2:ncol(dat)] <- table[2:nrow(table),2:ncol(table)]
        }
      }
    }
    row <- row + number_of_days(date)
    setTxtProgressBar(prog, value = i)
  }
  
  # Remove missing value codes and clean up column types
  dat <- .clean_table(dat, config, type, clean_names = TRUE,
                      remove_missing = TRUE, fix_types = TRUE)
  
  # Trim bad data
  dat <- try(.trim_data(dat))
  if (inherits(dat, "try-error")) {
    return("There is no good data in this file.")
  }

 dat
}
