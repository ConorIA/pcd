library(jsonlite)
library(plumber)
library(storr)
library(tibble)
source("plumber_settings.R")
st_pcd <- storr_rds("/data/pcd/")

#* If the request is coming from httr, then we will return serialized tibbles
#* @filter checkClient
function(req){
  message("Request from ", req$HTTP_USER_AGENT)
  req$client = ifelse(!grepl(".*r-curl.*httr.*", req$HTTP_USER_AGENT), "other", "r")
  message("Client set to ", req$client)
  forward()
}


#* Return the catalogue of available data
#* @serializer contentType list(type="application/octet-stream+json")
#* @get /catalogue
#* @param client Use R to return a tibble. Anything else will return JSON
function(req) {
  if (req$client == "r") {
    serialize(st_pcd$get("catalogue"), NULL)
    } else {
      toJSON(st_pcd$get("catalogue"))
    }
}


#* Return the requested table
#* @serializer contentType list(type="application/octet-stream+json")
#* @param station The station
#* @param year A vector of years to return
#* @param client Use R to return a tibble. Anything else will return JSON
#* @post /get
function(station, year = NULL, req, res) {
  
  station <- as.character(station)
  catalogue <- st_pcd$get("catalogue")
  
  if (nchar(station) < 6) {
    station <- suppressWarnings(try(sprintf("%06d", as.numeric(station)), silent = TRUE))
    if (inherits(station, "try-error") | !station %in% catalogue$StationID) {
      res$status <- 400
      res$body <- jsonlite::toJSON(auto_unbox = TRUE, list(
        status = 400,
        message = "Station ID appears invalid. Please try again."
      ))
      return(res)
    }
  }
  
  if (st_pcd$exists(station)) {
    message("Returning table from cache: ", station)
    dat <- st_pcd$get(station)
  } else {
    res$status <- 404
    res$body <- jsonlite::toJSON(auto_unbox = TRUE, list(
      status = 404,
      message = paste0("Station ", station, " does not exist in cache. Please check the the Station ID. \nIf you are sure the ID was correct, contact the package author.")
    ))
    return(res)
  }

  if (!is.null(year) && length(year) > 0) {
    dat <- dat[dat$Fecha >= paste0(min(year), "-01-01") &
                 dat$Fecha <= paste0(max(year), "-12-31"),]
  }
  
  if (req$client == "r") {
    serialize(dat, NULL)
  } else {
    toJSON(dat)
  }
}


#* Store the provided table
#* @param station The station
#* @post /set
function(req, res, station) {
  if (req$client != "r") {
    res$status <- 401 # Unauthorized
    res$body <- jsonlite::toJSON(auto_unbox = TRUE, list(
      status = 401,
      message = paste0("Only R is allowed")
    ))
    return(res)
  }
  auth = sub("Basic ", "", req$HTTP_AUTHORIZATION)
  if (!(rawToChar(jsonlite::base64_dec(auth)) %in% paste(users, passwords, sep = ":"))) {
    res$status <- 401 # Unauthorized
    res$body <- jsonlite::toJSON(auto_unbox = TRUE, list(
      status = 401,
      message = paste0("Authentication required")
    ))
    return(res)
  } else {
    message("Caching station ", station)
    tab = unserializeJSON(req$postBody)
    st_pcd$set(key = station, value = tab)
  }
}
