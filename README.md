PCD: Peruvian Climate Data from Senamhi made easy
================

This repository contains the code that is used to scrape, process, and
compile archived Peruvian climate and hydrology data from the [Peruvian
National Meteorology and Hydrology Service
(Senamhi)](https://www.senamhi.gob.pe/).

The code consists of two parts, an R package, that contains all the code
that is used to fetch the HTML tables of data from the Senamhi website
and package them up in a nicely readable table format, and a
[plumber](https://www.rplumber.io/) API that is used to host the data in
post-processed form.

## Using the data

### In R

This code was originally part of the *senamhiR* package and that package
remains the recommended way to access the data. If the request for the
data comes from the *httr* package (which is used by *senamhiR*), then
the plumber API will return a
tibble.

<span style="font-size:90%;color:#00000"><svg style="height:0.8em;top:.04em;position:relative;fill:black;" viewBox="0 0 581 512"><path d="M581 226.6C581 119.1 450.9 32 290.5 32S0 119.1 0 226.6C0 322.4 103.3 402 239.4 418.1V480h99.1v-61.5c24.3-2.7 47.6-7.4 69.4-13.9L448 480h112l-67.4-113.7c54.5-35.4 88.4-84.9 88.4-139.7zm-466.8 14.5c0-73.5 98.9-133 220.8-133s211.9 40.7 211.9 133c0 50.1-26.5 85-70.3 106.4-2.4-1.6-4.7-2.9-6.4-3.7-10.2-5.2-27.8-10.5-27.8-10.5s86.6-6.4 86.6-92.7-90.6-87.9-90.6-87.9h-199V361c-74.1-21.5-125.2-67.1-125.2-119.9zm225.1 38.3v-55.6c57.8 0 87.8-6.8 87.8 27.3 0 36.5-38.2 28.3-87.8 28.3zm-.9 72.5H365c10.8 0 18.9 11.7 24 19.2-16.1 1.9-33 2.8-50.6 2.9v-22.1z"/></svg>
<b>R</b></span>

``` r
library(senamhiR)
```

    ## The information accessed by this package was compiled and maintained by Peru's National Meteorology and Hydrology Service (Senamhi). The use of this data is of your sole responsibility.

<span style="font-size:90%;color:#00000"><svg style="height:0.8em;top:.04em;position:relative;fill:black;" viewBox="0 0 581 512"><path d="M581 226.6C581 119.1 450.9 32 290.5 32S0 119.1 0 226.6C0 322.4 103.3 402 239.4 418.1V480h99.1v-61.5c24.3-2.7 47.6-7.4 69.4-13.9L448 480h112l-67.4-113.7c54.5-35.4 88.4-84.9 88.4-139.7zm-466.8 14.5c0-73.5 98.9-133 220.8-133s211.9 40.7 211.9 133c0 50.1-26.5 85-70.3 106.4-2.4-1.6-4.7-2.9-6.4-3.7-10.2-5.2-27.8-10.5-27.8-10.5s86.6-6.4 86.6-92.7-90.6-87.9-90.6-87.9h-199V361c-74.1-21.5-125.2-67.1-125.2-119.9zm225.1 38.3v-55.6c57.8 0 87.8-6.8 87.8 27.3 0 36.5-38.2 28.3-87.8 28.3zm-.9 72.5H365c10.8 0 18.9 11.7 24 19.2-16.1 1.9-33 2.8-50.6 2.9v-22.1z"/></svg>
<b>R</b></span>

``` r
# An empty station_search() is the same as download_data("catalogue")
download_data("catalogue")
```

    ## # A tibble: 1,840 x 14
    ##    Station StationID Type  Configuration `Data Start` `Data End` `Period (Yr)`
    ##  * <chr>   <chr>     <chr> <chr>         <chr>        <chr>              <dbl>
    ##  1 ARAMAN… 000261    CON   M             1995         2020                  26
    ##  2 CHINGA… 152205    CON   M             1965         1996                  32
    ##  3 AMOJAO  221302    CON   H             1976         1981                   6
    ##  4 LA PECA 221508    CON   H             1965         1967                   3
    ##  5 IMACITA 000205    CON   M             1968         1980                  13
    ##  6 CHIRIA… 000229    CON   M             1993         2020                  28
    ##  7 SAN RA… 152222    CON   M             1965         1966                   2
    ##  8 JUMBIL… 152209    CON   M             1963         1974                  12
    ##  9 POMACO… 000254    CON   M             1964         1975                  12
    ## 10 JAZAN   000272    CON   M             1993         2019                  27
    ## # … with 1,830 more rows, and 7 more variables: `Station Status` <chr>,
    ## #   Latitude <dbl>, Longitude <dbl>, Altitude <int>, Region <chr>,
    ## #   Province <chr>, District <chr>

<span style="font-size:90%;color:#00000"><svg style="height:0.8em;top:.04em;position:relative;fill:black;" viewBox="0 0 581 512"><path d="M581 226.6C581 119.1 450.9 32 290.5 32S0 119.1 0 226.6C0 322.4 103.3 402 239.4 418.1V480h99.1v-61.5c24.3-2.7 47.6-7.4 69.4-13.9L448 480h112l-67.4-113.7c54.5-35.4 88.4-84.9 88.4-139.7zm-466.8 14.5c0-73.5 98.9-133 220.8-133s211.9 40.7 211.9 133c0 50.1-26.5 85-70.3 106.4-2.4-1.6-4.7-2.9-6.4-3.7-10.2-5.2-27.8-10.5-27.8-10.5s86.6-6.4 86.6-92.7-90.6-87.9-90.6-87.9h-199V361c-74.1-21.5-125.2-67.1-125.2-119.9zm225.1 38.3v-55.6c57.8 0 87.8-6.8 87.8 27.3 0 36.5-38.2 28.3-87.8 28.3zm-.9 72.5H365c10.8 0 18.9 11.7 24 19.2-16.1 1.9-33 2.8-50.6 2.9v-22.1z"/></svg>
<b>R</b></span>

``` r
download_data("000401")
```

    ## # A tibble: 8,401 x 13
    ##    Fecha      `Tmax (C)` `Tmin (C)` `TBS07 (C)` `TBS13 (C)` `TBS19 (C)`
    ##  * <date>          <dbl>      <dbl>       <dbl>       <dbl>       <dbl>
    ##  1 1998-01-01         NA         NA          NA          NA          NA
    ##  2 1998-01-02         NA         NA          NA          NA          NA
    ##  3 1998-01-03         NA         NA          NA          NA          NA
    ##  4 1998-01-04         NA         NA          NA          NA          NA
    ##  5 1998-01-05         NA         NA          NA          NA          NA
    ##  6 1998-01-06         NA         NA          NA          NA          NA
    ##  7 1998-01-07         NA         NA          NA          NA          NA
    ##  8 1998-01-08         NA         NA          NA          NA          NA
    ##  9 1998-01-09         NA         NA          NA          NA          NA
    ## 10 1998-01-10         NA         NA          NA          NA          NA
    ## # … with 8,391 more rows, and 7 more variables: `TBH07 (C)` <dbl>, `TBH13
    ## #   (C)` <dbl>, `TBH19 (C)` <dbl>, `Prec07 (mm)` <dbl>, `Prec19 (mm)` <dbl>,
    ## #   `Direccion del Viento` <chr>, `Velocidad del Viento (m/s)` <int>

### In Python

The plumber API will return a JSON table to any non-*httr* requests. As
such, the data can easily be read in Python using *requests*, and turned
into a *pandas*
df.

<span style="font-size:90%;color:#00000"><svg style="height:0.8em;top:.04em;position:relative;fill:black;" viewBox="0 0 448 512"><path d="M439.8 200.5c-7.7-30.9-22.3-54.2-53.4-54.2h-40.1v47.4c0 36.8-31.2 67.8-66.8 67.8H172.7c-29.2 0-53.4 25-53.4 54.3v101.8c0 29 25.2 46 53.4 54.3 33.8 9.9 66.3 11.7 106.8 0 26.9-7.8 53.4-23.5 53.4-54.3v-40.7H226.2v-13.6h160.2c31.1 0 42.6-21.7 53.4-54.2 11.2-33.5 10.7-65.7 0-108.6zM286.2 404c11.1 0 20.1 9.1 20.1 20.3 0 11.3-9 20.4-20.1 20.4-11 0-20.1-9.2-20.1-20.4.1-11.3 9.1-20.3 20.1-20.3zM167.8 248.1h106.8c29.7 0 53.4-24.5 53.4-54.3V91.9c0-29-24.4-50.7-53.4-55.6-35.8-5.9-74.7-5.6-106.8.1-45.2 8-53.4 24.7-53.4 55.6v40.7h106.9v13.6h-147c-31.1 0-58.3 18.7-66.8 54.2-9.8 40.7-10.2 66.1 0 108.6 7.6 31.6 25.7 54.2 56.8 54.2H101v-48.8c0-35.3 30.5-66.4 66.8-66.4zm-6.7-142.6c-11.1 0-20.1-9.1-20.1-20.3.1-11.3 9-20.4 20.1-20.4 11 0 20.1 9.2 20.1 20.4s-9 20.3-20.1 20.3z"/></svg>
<b>Python</b></span>

``` python
import requests
import pandas as pd

def download_data(station):
    if station == "catalogue":
        r = requests.get("https://api.conr.ca/pcd/catalogue")
    else:
        r = requests.post("https://api.conr.ca/pcd/get?station={}".format(station))
    return pd.read_json(r.content)
```

<span style="font-size:90%;color:#00000"><svg style="height:0.8em;top:.04em;position:relative;fill:black;" viewBox="0 0 448 512"><path d="M439.8 200.5c-7.7-30.9-22.3-54.2-53.4-54.2h-40.1v47.4c0 36.8-31.2 67.8-66.8 67.8H172.7c-29.2 0-53.4 25-53.4 54.3v101.8c0 29 25.2 46 53.4 54.3 33.8 9.9 66.3 11.7 106.8 0 26.9-7.8 53.4-23.5 53.4-54.3v-40.7H226.2v-13.6h160.2c31.1 0 42.6-21.7 53.4-54.2 11.2-33.5 10.7-65.7 0-108.6zM286.2 404c11.1 0 20.1 9.1 20.1 20.3 0 11.3-9 20.4-20.1 20.4-11 0-20.1-9.2-20.1-20.4.1-11.3 9.1-20.3 20.1-20.3zM167.8 248.1h106.8c29.7 0 53.4-24.5 53.4-54.3V91.9c0-29-24.4-50.7-53.4-55.6-35.8-5.9-74.7-5.6-106.8.1-45.2 8-53.4 24.7-53.4 55.6v40.7h106.9v13.6h-147c-31.1 0-58.3 18.7-66.8 54.2-9.8 40.7-10.2 66.1 0 108.6 7.6 31.6 25.7 54.2 56.8 54.2H101v-48.8c0-35.3 30.5-66.4 66.8-66.4zm-6.7-142.6c-11.1 0-20.1-9.1-20.1-20.3.1-11.3 9-20.4 20.1-20.4 11 0 20.1 9.2 20.1 20.4s-9 20.3-20.1 20.3z"/></svg>
<b>Python</b></span>

``` python
print(download_data("catalogue"))
```

    ##                Station StationID Type  ...    Region    Province    District
    ## 0             ARAMANGO    000261  CON  ...  AMAZONAS       BAGUA    ARAMANGO
    ## 1            CHINGANZA    152205  CON  ...  AMAZONAS       BAGUA    ARAMANGO
    ## 2               AMOJAO    221302  CON  ...  AMAZONAS       BAGUA    ARAMANGO
    ## 3              LA PECA    221508  CON  ...  AMAZONAS       BAGUA       BAGUA
    ## 4              IMACITA    000205  CON  ...  AMAZONAS       BAGUA       IMAZA
    ## ...                ...       ...  ...  ...       ...         ...         ...
    ## 1835          AGUAYTIA    000462  CON  ...   UCAYALI  PADRE ABAD  PADRE ABAD
    ## 1836       EL BOQUERON    109090  CON  ...   UCAYALI  PADRE ABAD  PADRE ABAD
    ## 1837   PUENTE AGUAYTIA    231201  CON  ...   UCAYALI  PADRE ABAD  PADRE ABAD
    ## 1838         SANTA ANA  47E8568A  SUT  ...   UCAYALI  PADRE ABAD  PADRE ABAD
    ## 1839  PUERTO ESPERANZA  47E87066  SUT  ...   UCAYALI       PURUS       PURUS
    ## 
    ## [1840 rows x 14 columns]

<span style="font-size:90%;color:#00000"><svg style="height:0.8em;top:.04em;position:relative;fill:black;" viewBox="0 0 448 512"><path d="M439.8 200.5c-7.7-30.9-22.3-54.2-53.4-54.2h-40.1v47.4c0 36.8-31.2 67.8-66.8 67.8H172.7c-29.2 0-53.4 25-53.4 54.3v101.8c0 29 25.2 46 53.4 54.3 33.8 9.9 66.3 11.7 106.8 0 26.9-7.8 53.4-23.5 53.4-54.3v-40.7H226.2v-13.6h160.2c31.1 0 42.6-21.7 53.4-54.2 11.2-33.5 10.7-65.7 0-108.6zM286.2 404c11.1 0 20.1 9.1 20.1 20.3 0 11.3-9 20.4-20.1 20.4-11 0-20.1-9.2-20.1-20.4.1-11.3 9.1-20.3 20.1-20.3zM167.8 248.1h106.8c29.7 0 53.4-24.5 53.4-54.3V91.9c0-29-24.4-50.7-53.4-55.6-35.8-5.9-74.7-5.6-106.8.1-45.2 8-53.4 24.7-53.4 55.6v40.7h106.9v13.6h-147c-31.1 0-58.3 18.7-66.8 54.2-9.8 40.7-10.2 66.1 0 108.6 7.6 31.6 25.7 54.2 56.8 54.2H101v-48.8c0-35.3 30.5-66.4 66.8-66.4zm-6.7-142.6c-11.1 0-20.1-9.1-20.1-20.3.1-11.3 9-20.4 20.1-20.4 11 0 20.1 9.2 20.1 20.4s-9 20.3-20.1 20.3z"/></svg>
<b>Python</b></span>

``` python
print(download_data("000401"))
```

    ##            Fecha  Tmax (C)  ...  Velocidad del Viento (m/s)  Direccion del Viento
    ## 0     1998-01-01       NaN  ...                         NaN                   NaN
    ## 1     1998-01-02       NaN  ...                         NaN                   NaN
    ## 2     1998-01-03       NaN  ...                         NaN                   NaN
    ## 3     1998-01-04       NaN  ...                         NaN                   NaN
    ## 4     1998-01-05       NaN  ...                         NaN                   NaN
    ## ...          ...       ...  ...                         ...                   ...
    ## 8396  2020-12-27       NaN  ...                         NaN                   NaN
    ## 8397  2020-12-28       NaN  ...                         NaN                   NaN
    ## 8398  2020-12-29       NaN  ...                         NaN                   NaN
    ## 8399  2020-12-30       NaN  ...                         NaN                   NaN
    ## 8400  2020-12-31       NaN  ...                         NaN                   NaN
    ## 
    ## [8401 rows x 13 columns]

### Anywhere else

The JSON-formatted data will be client agnostic and can be downloaded
directly to CSV through a program such as
[jq](https://stedolan.github.io/jq/).

<span style="font-size:90%;color:#00000"><svg style="height:0.8em;top:.04em;position:relative;fill:black;" viewBox="0 0 640 512"><path d="M257.981 272.971L63.638 467.314c-9.373 9.373-24.569 9.373-33.941 0L7.029 444.647c-9.357-9.357-9.375-24.522-.04-33.901L161.011 256 6.99 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L257.981 239.03c9.373 9.372 9.373 24.568 0 33.941zM640 456v-32c0-13.255-10.745-24-24-24H312c-13.255 0-24 10.745-24 24v32c0 13.255 10.745 24 24 24h304c13.255 0 24-10.745 24-24z"/></svg>
<b>Shell</b></span>

``` bash
curl -s -X GET "https://api.conr.ca/pcd/catalogue" | jq -r '.[0:10] | (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
```

    ## "Altitude","Configuration","Data End","Data Start","District","Latitude","Longitude","Period (Yr)","Province","Region","Station","Station Status","StationID","Type"
    ## 508,"M","2020","1995","ARAMANGO",-5.4199,-78.4355,26,"BAGUA","AMAZONAS","ARAMANGO","working","000261","CON"
    ## 578,"M","1996","1965","ARAMANGO",-5.4169,-78.4336,32,"BAGUA","AMAZONAS","CHINGANZA","closed","152205","CON"
    ## 476,"H","1981","1976","ARAMANGO",-5.4669,-78.4836,6,"BAGUA","AMAZONAS","AMOJAO","closed","221302","CON"
    ## 1450,"H","1967","1965","BAGUA",-5.6003,-78.4003,3,"BAGUA","AMAZONAS","LA PECA","closed","221508","CON"
    ## 300,"M","1980","1968","IMAZA",-5.0836,-78.3669,13,"BAGUA","AMAZONAS","IMACITA","closed","000205","CON"
    ## 323,"M","2020","1993","IMAZA",-5.1614,-78.2881,28,"BAGUA","AMAZONAS","CHIRIACO","working","000229","CON"
    ## 320,"M","1966","1965","IMAZA",-5.0669,-78.3669,2,"BAGUA","AMAZONAS","SAN RAFAEL","closed","152222","CON"
    ## 2284,"M","1974","1963","CHISQUILLA",-5.8836,-77.7503,12,"BONGARA","AMAZONAS","JUMBILLA","closed","152209","CON"
    ## 2181,"M","1975","1964","FLORIDA",-5.8003,-77.9169,12,"BONGARA","AMAZONAS","POMACOCHAS","closed","000254","CON"
    ## 1354,"M","2019","1993","JAZAN",-5.9448,-77.9757,27,"BONGARA","AMAZONAS","JAZAN","working","000272","CON"

<span style="font-size:90%;color:#00000"><svg style="height:0.8em;top:.04em;position:relative;fill:black;" viewBox="0 0 640 512"><path d="M257.981 272.971L63.638 467.314c-9.373 9.373-24.569 9.373-33.941 0L7.029 444.647c-9.357-9.357-9.375-24.522-.04-33.901L161.011 256 6.99 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L257.981 239.03c9.373 9.372 9.373 24.568 0 33.941zM640 456v-32c0-13.255-10.745-24-24-24H312c-13.255 0-24 10.745-24 24v32c0 13.255 10.745 24 24 24h304c13.255 0 24-10.745 24-24z"/></svg>
<b>Shell</b></span>

``` bash
curl -s -X POST "https://api.conr.ca/pcd/get?station=000401" | jq -r '.[396:406] | (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
```

    ## "Fecha","Prec07 (mm)","Prec19 (mm)","TBH07 (C)","TBH13 (C)","TBH19 (C)","TBS07 (C)","TBS13 (C)","Tmax (C)","Tmin (C)","Velocidad del Viento (m/s)"
    ## "1999-02-01",0.1,0,21.9,24.8,23.3,22.6,32.6,34,22,2
    ## "1999-02-02",0,0,22.3,23.4,22.4,24.8,28.5,32.2,23,2
    ## "1999-02-03",0,0,22.2,23.4,22,23.3,29,31.4,23,6
    ## "1999-02-04",0,0.8,21.2,24,22.9,22.2,28.6,30.5,21.8,4
    ## "1999-02-05",0,2.4,21.5,24.9,21.2,22,28.4,31,21.3,2
    ## "1999-02-06",3.1,0.2,21,22.4,23,21.6,25.6,27.5,21.3,
    ## "1999-02-07",0,0,20.4,23.6,22.2,21.8,26.6,29,19,2
    ## "1999-02-08",0,19.8,20.9,23.8,22,21.7,25.2,26,21.3,2
    ## "1999-02-09",0,0,20.4,24.4,23.4,20.8,29.7,32,20.5,2
    ## "1999-02-10",3.3,0,21.4,24.6,23.8,22,30.5,33,21.5,2

## Senamhi terms of use

Senamhi’s terms of use were originally posted
[here](http://www.senamhi.gob.pe/?p=0613), but that link is currently
redirecting to the Senamhi home page. However, the text of the terms was
identical to the
[terms](https://web.archive.org/web/20170721104800/http://www.peruclima.pe/?p=condiciones)
of Senamhi’s short-lived PeruClima website ([Google
translation](https://translate.google.com/translate?hl=&sl=es&tl=en&u=https%3A%2F%2Fweb.archive.org%2Fweb%2F20170721104800%2Fhttp%3A%2F%2Fwww.peruclima.pe%2F%3Fp%3Dcondiciones)).
The terms allow for the free and public access to information on their
website. Likewise, the data may be used in for-profit and non-profit
applications. However, Senamhi stipulates that any use of the data must
be accompanied by a disclaimer that Senamhi is the proprietor of the
information. The following text is recommended (official text in
Spanish):

  - **Official Spanish:** *Información recopilada y trabajada por el
    Servicio Nacional de Meteorología e Hidrología del Perú. El uso que
    se le da a esta información es de mi (nuestra) entera
    responsabilidad.*
  - **English translation:** This information was compiled and
    maintained by Peru’s National Meteorology and Hydrology Service
    (Senamhi). The use of this data is of my (our) sole responsibility.
